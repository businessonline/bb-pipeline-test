#!/bin/bash

function setError {
    SETERROR=$?
    echo "An exception was generated: ${SETERROR}"
    exit ${SETERROR}
}
trap setError ERR

for i in "$@"
do
case $i in
    --clientcode=*)
    CLIENTCODE="${i#*=}"
    shift # past argument=value
    ;;
    --pipelinestatus=*)
    PIPELINESTATUS="${i#*=}"
    shift # past argument=value
    ;;
    *)
            # unknown option
    ;;
esac
done

RUNTRACKERPATH="~/phoenix-redshift-etl"

### Remove directory if exists
rm -rf ${RUNTRACKERPATH}

# Retrieve repo
git clone git@bitbucket.org:businessonline/phoenix-runscript-tracker.git ${ETLPATH}

# Execute
cd ${RUNTRACKERPATH}
bash runscript_tracker.py --client-name ${CLIENTCODE} --status ${PIPELINESTATUS}

echo "done."
