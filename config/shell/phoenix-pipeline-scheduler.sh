#!/bin/bash

SETERROR=0
trap SETERROR=$? ERR

for i in "$@"
do
case $i in
    --jobgroup=*)
    JOBGROUP="${i#*=}"
    shift # past argument=value
    ;;
    *)
    # unknown option
    ;;
esac
done

PIPELINE_SCHEDULER_PATH="~/phoenix-pipeline-scheduler"


### Remove directory if exists
rm -rf ${PIPELINE_SCHEDULER_PATH}

# Retrieve phoenix-redshift-etl repo
git clone git@bitbucket.org:businessonline/phoenix-pipeline-scheduler.git ${PIPELINE_SCHEDULER_PATH}

cd ${PIPELINE_SCHEDULER_PATH}
python ./phoenix-pipeline-scheduler.py --job_group ${JOBGROUP}

if [ "$SETERROR" -ne 0 ]; then
    echo "An exception was generated: $SETERROR"
    exit $SETERROR
fi

echo "done."
