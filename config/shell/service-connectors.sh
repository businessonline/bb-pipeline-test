#!/bin/bash

for i in "$@"
do
case $i in
    --archive=*)
    ARCHIVE="${i#*=}"
    shift # past argument=value
    ;;
    --clientcode=*)
    CLIENTCODE="${i#*=}"
    shift # past argument=value
    ;;
    --enddate=*)
    ENDDATE="${i#*=}"
    shift # past argument=value
    ;;
    --startdate=*)
    STARTDATE="${i#*=}"
    shift # past argument=value
    ;;
    --repobranch=*)
    REPOBRANCH="${i#*=}"
    shift # past argument=value
    ;;
    *)
    # unknown option
    ;;
esac
done

SETERROR=false
ERRORMESSAGE=""
SCROOTPATH="~/phoenix-service-connectors"
SCPATH="/gooogle_analytics/get_ga_data.py"

PARAMS=""
if [ "${#STARTDATE}" -gt 0 ]; then
    if [ "${#ENDDATE}" -gt 0 ]; then
        PARAMS=" --start-date $STARTDATE --end-date $ENDDATE"
    else
        PARAMS=" --start-date $STARTDATE"
    fi
fi

if [ ${#ARCHIVE} -gt 0 ]; then
    PARAMS="${PARAMS} --archive $ARCHIVE"
fi

NOW=$(date +%Y-%m-%d)

### Add Environment Variables
export SCPATH
export CLIENTCODE
export REPOBRANCH

### Remove directory if exists
rm -rf ${SCROOTPATH}

#### Retrieve phoenix-service-connectors repo
git clone -b ${REPOBRANCH} git@bitbucket.org:businessonline/phoenix-service-connectors.git ${SCROOTPATH}

# CD to the root of connectors
cd ${SCROOTPATH}

## Copy s3 file to local
aws s3 cp s3://etldatapipeline/scripts/PipelineClients.txt pipelineclients.txt
## readin array  readarray a s3://etldatapipeline/scripts/PipelineClients.txt

## Read local file into array 'lines'
IFS=$'\n' read -d '' -r -a lines < pipelineclients.txt

## Output an array element
#echo "${lines[3]}"

## set temp variable value for testing CLIENTNAME=${LI%:*}
#CLIENTCODE="bol"
#echo ${CLIENTCODE}

#loop through array 'lines' and push client line into array of connectors 'CONNECTORS'
for line in "${lines[@]}"
    do
        CLIENTNAME=${line%:*}
        #echo "$CLIENTNAME"
        if [ "$CLIENTNAME" = "${CLIENTCODE}" ]; then
            CONNECTORSTRING=${line##*:} 
            #echo "$CONNECTORSTRING"
            # Create connectors array
            IFS=',' read -r -a CONNECTORS <<< "$CONNECTORSTRING"
            echo "First Element:"  ${CONNECTORS[0]}
        fi
    done
# If a valid array of connectors was generated loop though and run connector python code
if [ "${#CONNECTORS[@]}" -gt 0 ]; then
    # Loop connectors
    for con in "${CONNECTORS[@]}"
        do
            case $con in
                "ga")
                    cd google_analytics
                    python get_ga_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "ga_props")
                    cd google_analytics
                    python get_ga_data_wPROPS.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "ga_events")
                    cd google_analytics
                    python get_ga_data_events.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "ga_geo")
                    cd google_analytics
                    python get_ga_data_geo.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "mcf")
                    cd google_analytics
                    python get_mcf_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true 
                    cd ..
                    ;;
                "mcf_props")
                    cd google_analytics
                    python get_mcf_data_wPROPS.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true 
                    cd ..
                    ;;
                "gsa")
                    cd sa
                    python get_gsa_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "aw")
                    cd adwords
                    python get_aw_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "aw_props")
                    cd adwords
                    python get_aw_data_wPROPS.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "ac")
                    cd adcenter
                    python get_adcenter_data.py --client-code ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "dcs")
                    cd doubleclick
                    python get_dcs_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "hubspot")
                    cd hubspot
                    python get_company_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    python get_contact_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    python get_deals_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    cd ..
                    ;;
                "pardot")
                    cd paradot
                    python get_opportunity_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    python get_prospects_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    python get_campaign_data.py --client-name ${CLIENTCODE} || SETERROR=true
                    python get_visitoractivity_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "fbads_device")
                    cd facebook
                    python FacebookAdsInsights_2_5.py --client-name ${CLIENTCODE} --type channel --breakdowns device ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "fbads_gender")
                    cd facebook
                    python FacebookAdsInsights_2_5.py --client-name ${CLIENTCODE} --type channel --breakdowns gender ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "ftp")
                    cd ftp
                    python get_ftp_file.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "lkn")
                    cd linkedin
                    python get_adstage_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                "twitter")
                    cd linkedin
                    python get_adstage_twitter_data.py --client-name ${CLIENTCODE} ${PARAMS} || SETERROR=true
                    cd ..
                    ;;
                *) echo invalid option;;
                esac
        done
    
fi

if [ "$SETERROR" = true ]; then
    exit 1
fi

echo "done."
