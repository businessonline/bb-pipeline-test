#!/bin/bash

function setError {
    SETERROR=$?
    echo "An exception was generated: ${SETERROR}"
    # aws s3 cp s3://etldatapipeline/scripts/shell/runscript_tracker.sh runscript_tracker.sh
    # bash runscript_tracker.sh
    exit ${SETERROR}
}
trap setError ERR

for i in "$@"
do
case $i in
    --clientcode=*)
    CLIENTCODE="${i#*=}"
    shift # past argument=value
    ;;
    --repobranch=*)
    REPOBRANCH="${i#*=}"
    shift # past argument=value
    ;;
    *)
            # unknown option
    ;;
esac
done

# S3 Credentials
ACCESSKEY="AKIAJI2IRCRV4HLFHHDA"
SECRETACCESSKEY="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"

# Redshift Parametres
PGHOST="sage.ckb0xaplxxre.us-east-1.redshift.amazonaws.com"
PGPORT=5439
PGDATABASE="phoenix"
PGUSER="phoenix"
# PGOPTIONS='--client-min-messages=warning'

ETLPATH="~/phoenix-redshift-etl"

# Add Environment Variables
export PGHOST
export PGPORT
export PGDATABASE
export PGUSER

export REPOBRANCH
export ETLPATH

### Remove directory if exists
rm -rf ${ETLPATH}

# Retrieve phoenix-redshift-etl repo
git clone -b ${REPOBRANCH} git@bitbucket.org:businessonline/phoenix-redshift-etl.git ${ETLPATH}

# Find/replace key/values in repo
find ${ETLPATH} -type f -name "*.sh" -print0 | xargs -0 sed -i "s,{{client_name}},${CLIENTCODE},g"
find ${ETLPATH} -type f -name "*.sql" -print0 | xargs -0 sed -i "s,{{client_name}},${CLIENTCODE},g"
find ${ETLPATH} -type f -name "*.sql" -print0 | xargs -0 sed -i "s,{{access_key}},${ACCESSKEY},g"
find ${ETLPATH} -type f -name "*.sql" -print0 | xargs -0 sed -i "s,{{secret_access_key}},${SECRETACCESSKEY},g"

# Execute ETL
# bash ${ETLPATH}/run.sh

# CREATE
bash ${ETLPATH}/create.sh

# LOAD
bash ${ETLPATH}/load.sh

# Transform
bash ${ETLPATH}/transform.sh

# Views
bash ${ETLPATH}/views.sh

# Drop Temp
bash ${ETLPATH}/droptemp.sh

echo "done."
