#!/bin/bash

for i in "$@"
do
case $i in
    --clientcode=*)
    CLIENTCODE="${i#*=}"
    shift # past argument=value
    ;;
    *)
            # unknown option
    ;;
esac
done

# Redshift Parametres
PGHOST="sage.ckb0xaplxxre.us-east-1.redshift.amazonaws.com"
PGPORT=5439
PGDATABASE="phoenix"
PGUSER="phoenix"
PGPASSWORD="sEQ95fx2uYc4"
SLOTCOUNT=3

# Add Environment Variables
export PGHOST
export PGPORT
export PGDATABASE
export PGUSER
export PGPASSWORD

# sudo easy_install pip
# sudo yum -y install postgresql postgresql-devel gcc python-devel git
# sudo pip install PyGreSQL

git clone git@bitbucket.org:businessonline/amazon-redshift-utils.git ~/amazon-redshift-utils

if [ "${#CLIENTCODE}" -gt 0 ]; then
	python ~/amazon-redshift-utils/src/AnalyzeVacuumUtility/analyze-vacuum-schema.py --db ${PGDATABASE} --db-user ${PGUSER} --db-pwd ${PGPASSWORD} --db-host ${PGHOST} --db-port ${PGPORT} --schema-name ${CLIENTCODE} --output-file ~/vacuum_${CLIENTCODE}.log --slot-count ${SLOTCOUNT} --ignore-errors True
else
	python ~/amazon-redshift-utils/src/AnalyzeVacuumUtility/analyze_vacuum_schema_all.py --db_name ${PGDATABASE} --db_user ${PGUSER} --db_pwd ${PGPASSWORD} --db_host ${PGHOST} --db_port ${PGPORT} --slot_count ${SLOTCOUNT}
fi
